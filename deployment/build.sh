#!/bin/bash

export QMAKE=/shared/Qt/5.15.1/gcc_64/bin/qmake
export REPO_ROOT=~/zdrojaky/qarkanoid

rm -rf build
mkdir build
cd build

"$QMAKE" $REPO_ROOT
make -j$(nproc)
#make install INSTALL_ROOT=AppDir

