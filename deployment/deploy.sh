#!/bin/bash

export QMAKE=/shared/Qt/5.15.1/gcc_64/bin/qmake
export REPO_ROOT=~/zdrojaky/qarkanoid

cd build


export QML_SOURCES_PATHS="$REPO_ROOT"/qml
export EXTRA_QT_PLUGINS=multimedia
../../../linuxdeploy-x86_64.AppImage --appdir AppDir -e qArkanoid -i "$REPO_ROOT"/deployment/icon.png -d "$REPO_ROOT"/deployment/qArkanoid.desktop --plugin qt --output appimage
# ../../../linuxdeploy-x86_64.AppImage --appdir AppDir --output appimage

mv qArkanoid-*-x86_64.AppImage qArkanoid-x86_64.AppImage
