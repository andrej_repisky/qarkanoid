
TEMPLATE = app

QT += qml quick
CONFIG += c++17
CONFIG -= qtquickcompiler

SOURCES += \
    src/shared/Settings.cpp \
    src/main.cpp \
    src/MainObject.cpp \
    src/utils/AppSettings.cpp \
    src/utils/CDialog.cpp \
    src/MainQml.cpp \
    src/utils/QMLGlobal.cpp

HEADERS += \
    src/shared/Settings.h \
    src/shared/Singleton.h \
    src/MainObject.h \
    src/utils/AppSettings.h \
    src/utils/CDialog.h \
    src/MainQml.h \
    src/utils/QMLGlobal.h

RESOURCES += \
    resources/images.qrc \
    resources/sounds.qrc \
    qml/qml.qrc \
    resources/translations.qrc

# Default rules for deployment.
include(deployment.pri)

TRANSLATIONS += resources/translations/cs.ts \
                resources/translations/de.ts

DISTFILES += \
    qml/controls/CTextToolButton.qml \
    qml/home/HomePage.qml \
    qml/controls/CLabel.qml \
    qml/controls/CButton.qml \
    qml/controls/EmptyIndicator.qml \
    qml/controls/CRadioButton.qml \
    qml/controls/CComboBox.qml \
    qml/home/DarkTheme.qml \
    qml/home/LightTheme.qml \
    qml/home/MainUI.qml \
    qml/home/StackViewWrap.qml \
    qml/controls/SemitransparentRect.qml \
    qml/home/StackPage.qml \
    qml/controls/dialogs/YesNoDialog.qml \
    qml/controls/dialogs/Toast.qml \
    qml/game/arkanoid/Ball.qml \
    qml/game/arkanoid/BallItem.qml \
    qml/game/arkanoid/BoxBody.qml \
    qml/game/arkanoid/Brick.qml \
    qml/game/arkanoid/Dude.qml \
    qml/game/arkanoid/GameField.qml \
    qml/game/arkanoid/Wall.qml \
    qml/game/arkanoid/Game.qml \
    qml/game/ArkanoidMain.qml \
    qml/game/GamesPage.qml \
    qml/game/GameInfoItem.qml \
    qml/game/arkanoid/levels/Level1.qml \
    qml/game/arkanoid/levels/Level2.qml \
    qml/game/arkanoid/GameWrap.qml \
    qml/game/arkanoid/levels/DebugLevel2.qml \
    qml/game/arkanoid/levels/DebugLevel1.qml \
    qml/game/arkanoid/levels/Level3.qml \
    qml/game/arkanoid/levels/Level4.qml \
    qml/game/arkanoid/CollectItem.qml \
    qml/game/arkanoid/Enums.js \
    qml/game/arkanoid/levels/Level5.qml \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    qml/controls/Spinner.qml \
    qml/game/arkanoid/levels/BrickData.qml \
    qml/game/arkanoid/levels/GameLevelModel.qml \
    qml/controls/dialogs/OKDialog.qml \
    qml/controls/dialogs/DialogRectangle.qml \
    qml/home/main.qml
    
lupdate_only {                 # This code causes Qt Creator to show qml files as C++ sources in the project tree.
#   SOURCES += $$DISTFILES     # Uncomment this only while you run lupdate.
}


