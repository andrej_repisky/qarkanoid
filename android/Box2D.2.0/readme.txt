
Tento adresár obsahuje QML plugin Box2D zbildovaný v Qt 5.7 v release konfigurácii.

Tento adresár je treba nakopírovať do inštalačného adresára Qt na PC, podadresár android_armv7/qml.
Takže plná cesta bude napr. ~/Qt/5.7/android_armv7/qml, kde bude podadresár "Box2D.2.0".

Qt tento adresár automaticky v prípade potreby pribalí do .apk súboru.

Bildovanie qml-box2d pre Android:
Otvor v Qt Creatori box2d.pro a klikni na "Rebuild project".
