
import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.15
import MyStuff 1.0
import ".."

CDialog {
    signal yes()
    signal no()
    property string title
    property string text

    SemitransparentRect {
        anchors.fill: parent
//        Timer {
//            running:  true
//            interval: 100
//            onTriggered: {
//                parent.forceActiveFocus()
//            }
//        }
////        focus: true
//        Keys.onReleased: {
//            if(event.key === Qt.Key_Enter || event.key === Qt.Key_Return) {
//                _dialog.close()
//                _dialog.yes()
//                event.accepted = true
//            }
//        }
        DialogRectangle {
            height: 45 * mm
            ColumnLayout {
                anchors.fill: parent
                anchors.margins: 2 * mm
                spacing: 2 * mm
                Item {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Column {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        spacing: 3 * mm
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: _dialog.title
                            font.pixelSize: 5.1 * mm
                        }
                        Label {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            text: _dialog.text
                            font.pixelSize: 4.7 * mm
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }
                    Layout.fillHeight: true
                }

                RowLayout {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: 2 * mm
                    CButton {
                        text: qsTr("Yes")
                        onClicked: {
                            _dialog.close()
                            _dialog.yes()
                        }
                        Layout.fillWidth: true
                    }
                    CButton {
                        text: qsTr("No")
                        onClicked: {
                            _dialog.close()
                            _dialog.no()
                        }
                        Layout.fillWidth: true
                    }
                }
            }
        }
    }
}

