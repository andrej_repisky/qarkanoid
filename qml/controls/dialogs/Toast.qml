
import QtQuick 2.15
import QtQuick.Controls 1.4

Item {
    id: rootItem
	anchors.fill: parent
	property alias text: label.text
    property bool shown: false
    opacity: shown ? 1.0 : 0.0
    visible: opacity != 0.0
    onShownChanged: {
        if(shown === true)
            closeTimer.start()
    }

    Timer {
        id: closeTimer
        repeat: false
        interval: 3000
        onTriggered: {
            rootItem.shown = false
        }
    }
	Rectangle {
        color: "#d0ffffff"
        radius: 2 * mm
        anchors.centerIn: parent
        height: label.height + 6 * mm
        width: parent.width * 0.66
		Label {
			id: label
			anchors.verticalCenter: parent.verticalCenter
			anchors.left: parent.left
			anchors.right: parent.right
            anchors.margins: 2.5 * mm
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 4.7 * mm
			wrapMode: Text.WordWrap
		}
	}
    Behavior on opacity {
        NumberAnimation { duration: 150 }
    }
}
