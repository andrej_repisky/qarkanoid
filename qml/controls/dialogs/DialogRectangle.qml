
import QtQuick 2.15

Rectangle {
    id: root
    width: 85 * mm
    color: "#d0ffffff"
    radius: 2 * mm
    x: (parent.width - width) / 2
    y: parent.height

    Behavior on y {
        NumberAnimation {
            easing.type: Easing.OutQuad
            from: parent.height
            to: (parent.height - root.height) / 2
            duration: 250
        }
    }
}
