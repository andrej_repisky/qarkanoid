
import QtQuick 2.15
import QtQuick.Controls 1.4
import MyStuff 1.0

CLabel {
    id: rootItem
    property string title
    property variant model: ([])
    property string selectedKey: model[0].key
    onSelectedKeyChanged: {
        var i = 0
        for(; i < model.length; i ++) {
            if(model[i].key.toString() === selectedKey) {
                text = model[i].name
                break
            }
        }
        if(i === model.length)
            text = qsTr("vyplnit")
    }
    font.pixelSize: 4.5 * mm
    font.underline: rootItem.enabled
    color: rootItem.enabled ? rootGUIItem.theme.links.color : rootGUIItem.theme.inactiveTextColor
    MouseArea {
        anchors.fill: parent
        onClicked: dropDown.open()
    }
    CDialog {
        id: dropDown
        Rectangle {
            id: bgRect
            anchors.fill: parent
            color: "#80000000"
            MouseArea {
                anchors.fill: parent
            }
            Rectangle {
                anchors.centerIn: parent
                height: 55 * mm
                width: 75 * mm
                color: "#d0ffffff"
                radius: 2 * mm
                Column {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: 6 * mm
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 3 * mm
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: rootItem.title
                        font.pixelSize: 5.1 * mm
                    }
                    Item {
                        width: parent.width
                        height: 1 * mm
                    }

                    ExclusiveGroup {
                        id: comboBoxGroup
                    }
                    Repeater {
                        model: rootItem.model
                        CRadioButton {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            height: 6 * mm
                            text: modelData.name
                            checked: modelData.key.toString() === rootItem.selectedKey
                            exclusiveGroup: comboBoxGroup
                            onClicked: {
                                if(checked) {
                                    rootItem.selectedKey = modelData.key
                                }
                                _dialog.close()
                            }
                        }
                    }
                }
            }
        }
    }
}
