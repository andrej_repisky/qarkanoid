
import QtQuick 2.15
import QtQuick.Controls 1.4

Rectangle {
    id: rootItem
    property alias text: label.text
    property alias pressed: mouseArea.pressed
    signal clicked()
    color: mouseArea.pressed ? Qt.darker("#bbb") : "#bbb"
    height: 9.5 * mm
    width: label.width + 20 * mm
    radius: height
    clip: true
    Label {
        id: label
        anchors.centerIn: parent
        font.pixelSize: 4.3 * mm
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:
            rootItem.clicked()
    }
}

