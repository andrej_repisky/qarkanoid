
import QtQuick 2.15
import QtQuick.Controls 1.4

Item {
    id: rootItem
    property color color: "black"
    property alias text: label.text
    property bool checked: false
    signal clicked()
    property ExclusiveGroup exclusiveGroup: null
    onExclusiveGroupChanged: {
        if(exclusiveGroup)
            exclusiveGroup.bindCheckable(rootItem)
    }
    width: childrenRect.width
    Row {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        spacing: 2 * mm
        Rectangle {
            anchors.verticalCenter: parent.verticalCenter
            height: 4.3 * mm
            width: height
            radius: height
            color: "transparent"
            border.color: rootItem.color
            border.width: 0.6 * mm
            Rectangle {
                anchors.centerIn: parent
                visible: rootItem.checked
                height: 2.1 * mm
                width: height
                radius: height
                color: rootItem.color
            }
        }
        Label {
            id: label
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 4.7 * mm
            color: rootItem.color
            elide: Text.ElideRight
        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            rootItem.checked = !rootItem.checked
            rootItem.clicked()
        }
    }
}

