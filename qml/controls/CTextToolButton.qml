
import QtQuick 2.15
import QtGraphicalEffects 1.15
import QtQuick.Controls 1.4

Rectangle {
    id: rootItem
    height: 37 * mm
    width: height
    radius: 2 * mm
    color: theme.bgColor
    property alias text: label.text
    property alias iconSource: image.source
    signal clicked()
    Column {
        id: column
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        visible: !mouseArea.pressed
        clip: true
        spacing: 1 * mm
        Image {
            id: image
            anchors.horizontalCenter: parent.horizontalCenter
            height: rootItem.height * 0.5
            width: height
            fillMode: Image.PreserveAspectFit
        }
        Label {
            id: label
            anchors.left: parent.left
            anchors.right: parent.right
            font.pixelSize: rootItem.height * 0.14
            color: "white"
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
        }
    }
    ColorOverlay {
        anchors.fill: column
        source: column
        visible: !mouseArea.pressed && rootItem.enabled
        color: theme.fgColor
    }
    ColorOverlay {
        anchors.fill: column
        anchors.margins: -1 * mm
        visible: mouseArea.pressed
        source: column
        color: "#80a8bffb"
    }
    ColorOverlay {
        anchors.fill: column
        visible: !rootItem.enabled
        source: column
        color: "#50000000"
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:
            rootItem.clicked()
    }
}

