
import QtQuick 2.15

CLabel {
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.margins: 20 * mm
    font.pixelSize: 4.6 * mm
    font.italic: true
    color: "#bbb"
    wrapMode: Text.WordWrap
    horizontalAlignment: Text.AlignHCenter
}
