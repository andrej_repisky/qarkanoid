
import QtQuick 2.15
import QtQuick.Controls 1.4

Label {
    color: theme.fgColor
    textFormat: Text.StyledText
    font.pixelSize: 5 * mm
}

