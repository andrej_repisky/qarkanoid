
import QtQuick 2.15

QtObject {
    property color bgColor: "#3d3d3d"
    property color appBgColor: "#333333"
    property color fgColor: "white"
    property color inactiveTextColor: "lightgray"
    property QtObject links: QtObject {
        property color color: "lightblue"
    }
}
