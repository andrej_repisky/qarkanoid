

import QtQuick 2.15
import QtQuick.Controls 1.4
import MyStuff 1.0
import "../game"

Item {
    Rectangle {
        anchors.fill: parent
        color: theme.appBgColor
    }
    StackView {
        id: stackView       // To push items on stack, do not call stackView.push().
                            // Instead of that, inherit from StackPage and call StackPage.push().
        anchors.fill: parent
        focus: true
        clip: true

        initialItem: HomePage { id: homePage}

        Keys.onReleased: {
            if(event.key === Qt.Key_Back || event.key === Qt.Key_Escape || event.key === Qt.Key_Q) {
                if(stackView.depth > 1)
                    stackView.currentItem.requestClose()
                event.accepted = true
            }
            if(event.key === Qt.Key_N) {
                homePage.startGame()
                event.accepted = true
            }
        }
        Timer {
            running: true
            interval: 100
            onTriggered: {
                stackView.forceActiveFocus()
            }
        }
        delegate: StackViewDelegate {

            function transitionFinished(properties)
            {
                properties.exitItem.opacity = 1
            }

            pushTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                    duration: 500
                }
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                    duration: 500
                }
            }
        }
    }   // StackView
}
