
import QtQuick 2.15
import QtQuick.Controls 1.4
import "../controls"
import "../controls/dialogs"
import MyStuff 1.0

Item {
    id: rootGUIItem
    objectName: "rootGUIItem"
    StackViewWrap {
        anchors.fill: parent
        clip: true
    }

    function showToast(text) {
        toast.text = text
        toast.shown = true
    }
    Toast {
        id: toast
    }

    property alias theme: themeLoader.item
    Loader {
        id: themeLoader
        source: _settings.value("theme", "DarkTheme.qml")
        onSourceChanged:
            _settings.setValue("theme", source)
    }
}
