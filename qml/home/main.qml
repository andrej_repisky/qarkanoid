
import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Window 2.9

ApplicationWindow {
    id: rootWindow
    title: "qArkanoid"
    width: 190 * mm
    height: 151 * mm
    visible: true

    MainUI {
        id: mainUI
        anchors.fill: parent
    }
}
