
import QtQuick 2.15
import QtQuick.Layouts 1.15
import MyStuff 1.0
import "../controls"
import ".."
import "../game"

Item {
    function startGame() {
        gamePage.push()
    }
    GridLayout {
        id: gridLayout
        anchors.centerIn: parent
        rowSpacing: 5 * mm
        columnSpacing: rowSpacing
        columns: 3
        Timer {
            running: _constants.isDebugBuild
            interval: 1
            onTriggered:
                gamePage.push()
        }
        CTextToolButton {
            text: qsTr("<u>N</u>ew game")
            iconSource: "qrc:/images/new_game.png"
            onClicked: {
                gamePage.push()
            }

            GamesPage {
                id: gamePage
            }
        }

        CTextToolButton {
            text: qsTr("Settings")
            iconSource: "qrc:/images/settings.png"
            enabled: false
        }
        CTextToolButton {
            text: qsTr("About")
            iconSource: "qrc:/images/information.png"
            onClicked: {
                aboutPage.push()
            }
            About {
                id: aboutPage
            }
        }
    }
    CLabel {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 5 * mm
        horizontalAlignment: Text.AlignHCenter
        color: "gray"
        textFormat: Text.MarkdownText
        text: qsTr("
**Game controls:**  
Enter: starts the ball  
arrow keys are moving the trolley  
Esc, Q: leaves the game  
P: pauses the game  
")
    }
}

