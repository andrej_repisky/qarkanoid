
import QtQuick 2.15
import QtQuick.Controls 1.4

QtObject {
    id: root
    property string title: "Strana"
    property Item page: null
    signal closed()
    default property Component pageComponent
    property Component wrapComponent: Item {
        property string title: ""
        Loader {
            anchors.fill: parent
            sourceComponent: pageComponent
        }

        function requestClose() {
            root.requestClose()
        }
        function close() {
            if(stackView.currentItem === root.page)
                stackView.pop()
            root.closed()
        }
        Stack.onViewChanged: {
            if(Stack.view === null)
                root.page.destroy()
        }
    }

    function push() {
        if(root.page === null) {
            page = wrapComponent.createObject()
            page.title = root.title
            stackView.push(page)
        }
    }
    function close() {
        if(root.page !== null)
            page.close()
    }
    function requestClose() {  // Override this function in StackPage-derived element if necessary.
        close()
    }
}

