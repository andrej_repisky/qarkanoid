
import QtQuick 2.15

QtObject {
    property color bgColor: "lightgray"
    property color appBgColor: "#f0e0e0"
    property color fgColor: "black"
    property color inactiveTextColor: "#555"
    property QtObject links: QtObject {
        property color color: "blue"
    }
}
