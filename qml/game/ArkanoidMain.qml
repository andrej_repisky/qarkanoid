
import QtQuick 2.15
import "../controls"
import "arkanoid"

Item {
    anchors.fill: parent
    property real captionMargins: 1 * mm
    property real captionSpacing: 6.2 * mm

    Keys.onReleased: {
        if(event.key === Qt.Key_P) {
            if(game.isOver)
                return
            game.paused = !game.paused
            event.accepted = true
        }
    }
    Rectangle {
        id: gameRect
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: height
        color: "black"
        border.width: 0.7 * mm
        border.color: "white"
        radius: 2 * mm
        clip: true
        GameWrap {
            id: game
            anchors.fill: parent
            anchors.margins: 1.0 * mm
        }
    }

    Column {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: gameRect.right
        anchors.margins: parent.captionMargins
        spacing: parent.captionSpacing
        clip: true
        GameInfoItem {
            title: qsTr("Lives")
            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 1.2 * mm
                layoutDirection: Qt.RightToLeft
                Repeater {
                    model: game.remainingLives
                    BallItem {
                    }
                }
            }
        }
        GameInfoItem {
            title: qsTr("Score")
            CLabel {
                anchors.horizontalCenter: parent.horizontalCenter
                text: game.score
                font.pixelSize: 7 * mm
            }
        }
        GameInfoItem {
            title: qsTr("Level")
            CLabel {
                anchors.horizontalCenter: parent.horizontalCenter
                text: game.currentLevelIndex
                font.pixelSize: 7 * mm
            }
        }
        GameInfoItem {
            visible: game.stretchTime > 0
            title: qsTr("Stretch")
            CLabel {
                anchors.horizontalCenter: parent.horizontalCenter
                text: game.stretchTime + " s"
                font.pixelSize: 7 * mm
            }
        }
    }
}
