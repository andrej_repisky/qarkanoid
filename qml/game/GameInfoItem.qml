
import QtQuick 2.15
import "../controls"

Column {
    anchors.horizontalCenter: parent.horizontalCenter
    width: 33 * mm
    height: 20 * mm
    property alias title: heading.text
    spacing: 2 * mm
    CLabel {
        id: heading
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: _constants.heading_size_mm * mm
    }
}
