
import QtQuick 2.15
import "../home"
import "../controls/"

StackPage {
    Item {
        anchors.fill: parent
        anchors.margins: 5 * mm
        clip: true
        
        CLabel {
            id: licenseLabel
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            textFormat: Text.RichText
            font.pixelSize: 4.5 * mm
            horizontalAlignment: Text.AlignJustify
            wrapMode: Text.WordWrap
            onLinkActivated: Qt.openUrlExternally(link)
            text: qsTr("
<h2 align=center> qArkanoid </h2>
<p>
Copyright 2020 Andrej Repiský 
</p><p>
This program is free software: you can redistribute it and/or modify
it under the terms of the European Union Public License, version 1.2.
</p><p>
This program is distributed in the hope that it will be useful,
but with absolutely no warranty. See the enclosed EUPL for more details.
</p>
<h3 align=center> Third-party software </h3>
<p>
This program uses the following third-party libraries:
<ul>
<li>Qt 5.15 (license: GNU GPL, version 3)</li>
<li><a href=\"https://box2d.org/\">Box2D 2.0</a> (Copyright 2020 Erin Catto)</li>
</ul>
</p>

<h3 align=center> Media </h3>
<p>Icons are downloaded from <a href=\"https://flyclipart.com\">https://flyclipart.com</a> under Creative Commons BY-NC 4.0 license. </p>
<p>Sounds are downloaded from <a href=\"http://www.freesound.org/people/kantouth/sounds/106727/\">www.freesound.org</a> under Creative Commons Attribution 3.0 Unported (CC BY 3.0) license. </p>
")
        }
        CLabel {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            color: "gray"
            font.pixelSize: 4.5 * mm
            text: _settings.versionText()
        }
    }
}


