
import QtQuick 2.15
import "../home"
import "../controls/dialogs"

StackPage {
    id: rootPage
    Loader {
        source: "ArkanoidMain.qml"
        asynchronous: true
    }
}
