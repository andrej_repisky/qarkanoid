
import QtQuick 2.15

GameLevelModel {

    BrickData {
        column: 3
        row: 0
    }

    // next row
    BrickData {
        column: 2
        row: 1
    }
    BrickData {
        column: 3
        row: 1
    }
    BrickData {
        column: 4
        row: 1
    }

    // next row
    BrickData {
        column: 1
        row: 2
    }
    BrickData {
        column: 2
        row: 2
    }
    BrickData {
        column: 3
        row: 2
    }
    BrickData {
        column: 4
        row: 2
    }
    BrickData {
        column: 5
        row: 2
    }

    // next row
    BrickData {
        column: 0
        row: 3
    }
    BrickData {
        column: 1
        row: 3
    }
    BrickData {
        column: 2
        row: 3
    }
    BrickData {
        column: 3
        row: 3
    }
    BrickData {
        column: 4
        row: 3
    }
    BrickData {
        column: 5
        row: 3
    }
    BrickData {
        column: 6
        row: 3
    }

    // next row
    BrickData {
        column: 1
        row: 4
    }
    BrickData {
        column: 2
        row: 4
    }
    BrickData {
        column: 3
        row: 4
    }
    BrickData {
        column: 4
        row: 4
    }
    BrickData {
        column: 5
        row: 4
    }

    // next row
    BrickData {
        column: 2
        row: 5
    }
    BrickData {
        column: 3
        row: 5
    }
    BrickData {
        column: 4
        row: 5
    }

    // next row
    BrickData {
        column: 3
        row: 6
    }

    // next row
    BrickData {
        column: 0
        row: 9
    }
//    BrickData {
//        column: 1
//        row: 9
//    }
    BrickData {
        column: 2
        row: 9
    }
//    BrickData {
//        column: 3
//        row: 9
//    }
    BrickData {
        column: 4
        row: 9
    }
//    BrickData {
//        column: 5
//        row: 9
//    }
    BrickData {
        column: 6
        row: 9
    }

    // next row
    BrickData {
        column: 0
        row: 10
    }
    BrickData {
        column: 1
        row: 10
        persist: true
    }
    BrickData {
        column: 2
        row: 10
    }
    BrickData {
        column: 3
        row: 10
        persist: true
    }
    BrickData {
        column: 4
        row: 10
    }
    BrickData {
        column: 5
        row: 10
        persist: true
    }
    BrickData {
        column: 6
        row: 10
    }

    // next row
    BrickData {
        column: 0
        row: 11
    }
//    BrickData {
//        column: 1
//        row: 11
//    }
    BrickData {
        column: 2
        row: 11
    }
//    BrickData {
//        column: 3
//        row: 11
//    }
    BrickData {
        column: 4
        row: 11
    }
//    BrickData {
//        column: 5
//        row: 11
//    }
    BrickData {
        column: 6
        row: 11
    }
}
