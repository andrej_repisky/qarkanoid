
import QtQuick 2.15

GameLevelModel {

    BrickData {
        column: 0
        row: 2
    }

    // next row
    BrickData {
        column: 0
        row: 3
    }
    BrickData {
        column: 1
        row: 3
    }

    // next row
    BrickData {
        column: 0
        row: 4
    }
    BrickData {
        column: 1
        row: 4
    }
    BrickData {
        column: 2
        row: 4
    }

    // next row
    BrickData {
        column: 0
        row: 5
    }
    BrickData {
        column: 1
        row: 5
    }
    BrickData {
        column: 2
        row: 5
    }
    BrickData {
        column: 3
        row: 5
    }

    // next row
    BrickData {
        column: 0
        row: 6
    }
    BrickData {
        column: 1
        row: 6
    }
    BrickData {
        column: 2
        row: 6
    }
    BrickData {
        column: 3
        row: 6
    }
    BrickData {
        column: 4
        row: 6
    }

    // next row
    BrickData {
        column: 0
        row: 7
    }
    BrickData {
        column: 1
        row: 7
    }
    BrickData {
        column: 2
        row: 7
    }
    BrickData {
        column: 3
        row: 7
    }
    BrickData {
        column: 4
        row: 7
    }
    BrickData {
        column: 5
        row: 7
    }

    // next row
    BrickData {
        column: 0
        row: 8
    }
    BrickData {
        column: 1
        row: 8
    }
    BrickData {
        column: 2
        row: 8
    }
    BrickData {
        column: 3
        row: 8
    }
    BrickData {
        column: 4
        row: 8
    }
    BrickData {
        column: 5
        row: 8
    }
    BrickData {
        column: 6
        row: 8
    }

    // next row
    BrickData {
        column: 0
        row: 9
    }
    BrickData {
        column: 1
        row: 9
    }
    BrickData {
        column: 2
        row: 9
    }
    BrickData {
        column: 3
        row: 9
    }
    BrickData {
        column: 4
        row: 9
    }
    BrickData {
        column: 5
        row: 9
    }
    BrickData {
        column: 6
        row: 9
    }
}
