
import QtQuick 2.15
import "../Enums.js" as Enums

QtObject {
    id: level
    property list<BrickData> brickList
    default property alias bricksProperty: level.brickList
    property variant outputList
    property int removableBricksCount: 0

    Component.onCompleted: {
        //console.log("bricks: " + brickList.length)
        var oldBricks = brickList
        for(var i = 1; i < oldBricks.length; i++) {
            if((i % parseInt((Math.random() * 50), 10)) == 0) {
                var collectType = Enums.CollectType.Life
                if(Math.random() < 0.8)
                    collectType = Enums.CollectType.AddPoints
                if(Math.random() < 0.3)
                    collectType = Enums.CollectType.Stretch
                oldBricks[i].collectType = collectType
            }
        }
        //console.log(JSON.stringify(oldBricks))
        outputList = oldBricks
        for(i = 0; i < outputList.length; i++) {
            if(outputList[i].persist === false)
                removableBricksCount ++
        }
    }
}
