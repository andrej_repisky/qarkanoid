
import QtQuick 2.15

GameLevelModel {

    BrickData {
        column: 1
        row: 3
    }
    BrickData {
        column: 2
        row: 3
    }
    BrickData {
        column: 4
        row: 3
    }
    BrickData {
        column: 5
        row: 3
    }

    BrickData {
        column: 1
        row: 4
    }
    BrickData {
        column: 2
        row: 4
    }
    BrickData {
        column: 4
        row: 4
    }
    BrickData {
        column: 5
        row: 4
    }

    BrickData {
        column: 1
        row: 5
    }
    BrickData {
        column: 2
        row: 5
    }
    BrickData {
        column: 3
        row: 5
    }
    BrickData {
        column: 4
        row: 5
    }
    BrickData {
        column: 5
        row: 5
    }

    BrickData {
        column: 1
        row: 6
        persist: true
    }
    BrickData {
        column: 2
        row: 6
        persist: true
    }
    BrickData {
        column: 3
        row: 6
        persist: true
    }
    BrickData {
        column: 4
        row: 6
        persist: true
    }
    BrickData {
        column: 5
        row: 6
        persist: true
    }

    BrickData {
        column: 1
        row: 7
    }
    BrickData {
        column: 2
        row: 7
    }
    BrickData {
        column: 3
        row: 7
    }
    BrickData {
        column: 4
        row: 7
    }
    BrickData {
        column: 5
        row: 7
    }

    BrickData {
        column: 1
        row: 8
    }
    BrickData {
        column: 2
        row: 8
    }
    BrickData {
        column: 4
        row: 8
    }
    BrickData {
        column: 5
        row: 8
    }
    BrickData {
        column: 1
        row: 9
    }
    BrickData {
        column: 2
        row: 9
    }
    BrickData {
        column: 4
        row: 9
    }
    BrickData {
        column: 5
        row: 9
    }

    BrickData {
        column: 1
        row: 10
    }
    BrickData {
        column: 2
        row: 10
    }
    BrickData {
        column: 4
        row: 10
    }
    BrickData {
        column: 5
        row: 10
    }
}
