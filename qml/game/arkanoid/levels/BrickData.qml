
import QtQuick 2.15

ListElement {
    property int row: 0
    property int column: 0
    property int collectType: 0
    property bool persist: false
}

