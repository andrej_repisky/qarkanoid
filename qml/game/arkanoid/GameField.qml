
import QtQuick 2.15
import QtMultimedia 5.15
import "Enums.js" as Enums
import "levels"

Item {
    id: rootItem
    property size fieldSize: Qt.size(7, 20)   // počet tehličiek na šírku, počet tehličiek na výšku
    property size brickSize: Qt.size(rootItem.width / fieldSize.width, rootItem.height / fieldSize.height)
    height: parent.height
    property int currentLevelIndex: _constants.isDebugBuild ? 3 : 1
    property GameLevelModel currentLevel: createCurrentLevel(currentLevelIndex)
    signal levelCompleted()

    function createCurrentLevel(index) {
        var fileName = "./levels/Level"
//        var fileName = _constants.isDebugBuild ? "./levels/DebugLevel" : "./levels/Level"
        fileName += index + ".qml"
        var component = Qt.createComponent(fileName)
        if(component.status === Component.Ready) {
            return component.createObject(rootItem)
        }
        else if(component.status === Component.Error) {
            console.log(component.errorString())
            rootGUIItem.showToast("Žádné další úrovně už ve hře nejsou.")
            gameFinishedSound.play()
            return null
        }
    }

    Repeater  {
        id: brickRepeater
        anchors.fill: parent
        model: currentLevel.outputList
        delegate: Brick {
            width: rootItem.brickSize.width
            height: rootItem.brickSize.height
            x: rootItem.brickSize.width * modelData.column
            y: rootItem.brickSize.height * modelData.row
            collectType: modelData.collectType
            persist: modelData.persist
            onRemoved: {
                currentLevel.removableBricksCount --
                if(currentLevel != null && currentLevel.removableBricksCount === 0) {
                    victorySound.play()
                    rootItem.levelCompleted()
                }
            }
        }
    } // Repeater

    Audio {
        id: victorySound
        source: _soundPath + "/victory.wav"
    }
    Audio {
        id: gameFinishedSound
        source: _soundPath + "/victory-brass.wav"
    }
}
