
import QtQuick 2.15
import Box2D 2.0
import "Enums.js" as Enums

Item {
    id: rootItem
    property real brickMargin: 0.6 * mm
    property int collectType: 0
    property bool persist: false
    signal removed()

    Rectangle {
        anchors.fill: parent
        anchors.margins: brickMargin
        color: {
            if(parent.persist === true)
                return "transparent"
            return (collectType === 0) ? "lightgray" : game.activeBrickColor
        }
        antialiasing: true
        border.color: "lightgray"
        border.width: parent.persist ? 0.5 * mm : 0
        radius: 1 * mm
    }

    BoxBody {
        id: body
        active: rootItem.visible
        target: rootItem
        world: physicsWorld

        width: rootItem.width
        height: rootItem.height
        bodyType: Body.Static
        onEndContact: {
            if(other.objectName === "ball") {
                if(rootItem.persist === false) {
                    game.score += 10
                    rootItem.releaseCollectItem()
                    rootItem.remove()
                }
            }
        }
    }
    function remove() {
        visible = false
        rootItem.removed()
    }

    function releaseCollectItem() {
        if(rootItem.collectType === 0)
            return
        var component = Qt.createComponent("CollectItem.qml")
        if(component.status === Component.Ready) {
            var collectItem = component.createObject(game, {"y": rootItem.y, "collectType": rootItem.collectType})
            collectItem.x = rootItem.x + (rootItem.width - collectItem.width) / 2
        }
    }
}
