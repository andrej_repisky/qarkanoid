
import QtQuick 2.15
import QtQuick.Controls 1.4
import Box2D 2.0
import "Enums.js" as Enums

Rectangle {
    id: rootItem
    width: 23 * mm
    height: 8 * mm
    radius: 1.8 * mm
    color: game.activeBrickColor
    opacity: 0.8

    property int collectType

    Label {
        anchors.centerIn: parent
        text: {
            switch(collectType) {
            case Enums.CollectType.Life:
                return qsTr("LIFE")
            case Enums.CollectType.AddPoints:
                return "+200"
            case Enums.CollectType.Stretch:
                return "<          >"
            }
        }
        horizontalAlignment: Text.AlignJustify
        font.bold: true
        font.pixelSize: 5.5 * mm
    }

    BoxBody {
        target: rootItem
        world: physicsWorld
        width: rootItem.width
        height: rootItem.height

        linearVelocity: Qt.point(0, 2 * mm)
        bodyType: Body.Dynamic
        sensor: true
        fixtureObjectName: "collectItem"
    }

    Component.onCompleted:
        rootItem.parent.runningChanged.connect(checkRunning)

    function checkRunning() {
        if(rootItem && !rootItem.parent.running) {
            rootItem.destroy()
        }
    }
}
