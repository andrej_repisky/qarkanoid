
import QtQuick 2.15
import QtQuick.Controls 1.4
import "../../controls"

Game {
    id: game
    Rectangle {
        visible: pauseLabel.visible || game.isOver
        anchors.fill: parent
        anchors.margins: -0.5 * mm
        color: "#b0808080"
    }
    CLabel {
        visible: game.isOver
        text: qsTr("Game over")
        anchors.centerIn: parent
        font.pixelSize: 12 * mm
    }
    CLabel {
        id: pauseLabel
        visible: game.paused && !_constants.isDebugBuild
        text: qsTr("Pause")
        anchors.centerIn: parent
        font.pixelSize: 12 * mm
    }
    onCurrentLevelIndexChanged: {
        showLevelDisplay.start()
    }
    Label {
        id: levelDisplay
        anchors.centerIn: parent
        font.pixelSize: 10 * mm
        color: "white"
        text: qsTr("Level %1").arg(game.currentLevelIndex)
        opacity: 0
    }

    ParallelAnimation {
        id: showLevelDisplay
        property int duration: 1000
        NumberAnimation {
            property: "scale"
            from: 1.0
            to: 3.0
            duration: showLevelDisplay.duration
            target: levelDisplay
        }
        SequentialAnimation {
            NumberAnimation {
                property: "opacity"
                from: 0
                to: 1.0
                duration: 20
                target: levelDisplay
            }
            NumberAnimation {
                property: "opacity"
                from: 1.0
                to: 0
                duration: showLevelDisplay.duration
                target: levelDisplay
            }
        }
    }
}
