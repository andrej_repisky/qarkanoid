
import QtQuick 2.15
import QtMultimedia 5.15
import Box2D 2.0
import "Enums.js" as Enums
import MyStuff 1.0

Rectangle {
    id: rootItem
    width: (rootItem.stretchTime > 0) ? 55 * mm : 40 * mm
    height: 7 * mm
    color: "lightblue"
    radius: 1 * mm

    property int maxX: 0
    property int centerX: 0
    onCenterXChanged:
        updateX()
    onWidthChanged: {
        updateX()
    }
    function updateX() {
        rootItem.x = centerX - (rootItem.width / 2)
    }
    onXChanged: {
        centerX = rootItem.x + (rootItem.width / 2)
        if(x <= 0 || x >= maxX)
            boxBody.setSpeed(0)
        if(x < 0)
            x = 0
        if(x > maxX)
            x = maxX
    }

    property int stretchTime: 0
    Timer {
        id: stretchTimer
        running: (rootItem.stretchTime > 0) && !game.paused
        interval: 1000
        repeat: true
        onTriggered: {
            rootItem.stretchTime --
        }
    }

    Connections {
        target: game
        function onBallLost() {
            rootItem.stretchTime = 0
        }
    }

    Body {
        id: boxBody
        world: physicsWorld
        target: rootItem
        bodyType: Body.Kinematic
        fixtures: Box {
            id: box
            objectName: "dude"
            density: 1
            friction: 0.0
            restitution: 0.0
            width: rootItem.width
            height: rootItem.height
            onBeginContact: {
                if(other.objectName === "collectItem") {
                    var collectType = other.getBody().target.collectType
                    catchAudio.stop()
                    catchAudio.play()
                    other.getBody().target.destroy()
                    switch(collectType) {
                    case Enums.CollectType.Life:
                        if(game.remainingLives < game.maximumLives)
                            game.remainingLives ++
                        break
                    case Enums.CollectType.AddPoints:
                        game.score += 200
                        break
                    case Enums.CollectType.Stretch:
                        rootItem.stretchTime += 20
                        break
                    }
                }
            }
        }
        property real maxSpeed: 40
        function setSpeed(ratio) {
            var xSpeed = ratio * maxSpeed
            if(xSpeed > 0 && x >= maxX)
                xSpeed = 0
            if(xSpeed < 0 && x <= 0)
                xSpeed = 0
            linearVelocity = Qt.point(xSpeed, 0)
        }
    }

    Audio {
        id: catchAudio
        source: _soundPath + "/success.wav"
    }

    Timer {
        running:  true
        interval: 200
        onTriggered: {
            rootItem.centerX = game.width / 2
            parent.forceActiveFocus()
        }
    }
    Keys.onLeftPressed: {
        boxBody.setSpeed(-0.5)
        event.accepted = true
    }
    Keys.onRightPressed: {
        if(rootItem.x < (maxX - 1))
            boxBody.setSpeed(0.5)
        event.accepted = true
//        console.log("maxX: " + maxX + " x: " + x)
    }
    Keys.onReleased: {
        boxBody.setSpeed(0)
    }

    Behavior on width {
        NumberAnimation {
            properties: "width"
            duration: 150
        }
    }
}
