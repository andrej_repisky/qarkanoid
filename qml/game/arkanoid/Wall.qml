

import QtQuick 2.15

Item {
    id: wall

    BoxBody {
        target: wall
        world: physicsWorld

        width: wall.width
        height: wall.height
    }
}
