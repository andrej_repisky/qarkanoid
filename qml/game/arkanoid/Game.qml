
import QtQuick 2.15
import QtMultimedia 5.15
import Box2D 2.0

Item {
    id: game
    property bool running: false
    property bool paused: false
    property bool isOver: false
    property int score: _constants.isDebugBuild ? 1610 : 0
    property int remainingLives: _constants.isDebugBuild ? 4 : 2
    property int maximumLives: 8
    property alias stretchTime: dude.stretchTime
    property alias currentLevelIndex: gameField.currentLevelIndex
    property color activeBrickColor: "#ffa0a0"

    signal ballLost()

    onRunningChanged: {
        if(running)
            ball.startUpwards(Math.random())
        else {
            if(remainingLives > 0) {
                ball.reset()
            } else {
                isOver = true
                gameOverSound.play()
            }
        }
    }
    Audio {
        id: gameOverSound
        source: _soundPath + "/dark-bell.wav"
    }
    World {
        id: physicsWorld
        gravity: Qt.point(0, 0)
        running: !(game.paused || game.isOver)
        onPreSolve: {
            if(!game.running)
                return
            var targetA = contact.fixtureA.getBody().target
            var targetB = contact.fixtureB.getBody().target
            if(targetA === dude && targetB === ball) {
                contact.enabled = false
                var collisionX = ball.x + ball.width/2 - dude.x
                var collRatio = collisionX / dude.width
                ball.startUpwards(collRatio)
                return
            }
        }
    }
    Audio {
        id: lostBallSound
        source: _soundPath + "/fail.wav"
    }
    Item {
        id: ground
        height: 20
        anchors { left: parent.left; right: parent.right; top: parent.bottom }
        BoxBody {
            target: ground
            world: physicsWorld
            sensor: true
            width: ground.width
            height: ground.height
            onEndContact: {
                if(!game.running)
                    return
                if(other.getBody().target === ball) {
                    game.remainingLives --
                    game.ballLost()
                    game.running = false
                    if(game.remainingLives > 0)
                       lostBallSound.play()
                }
            }
        }
    }

    Wall {
        id: ceiling
        height: ground.height
        anchors { left: parent.left; right: parent.right; bottom: parent.top }
    }
    Wall {
        id: leftWall
        width: ground.height
        anchors { right: parent.left; bottom: parent.bottom; top: ceiling.bottom }
    }
    Wall {
        id: rightWall
        width: ground.height
        anchors { left: parent.right; bottom: parent.bottom; top: ceiling.bottom }
    }
    GameField {
        id: gameField
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        onLevelCompleted: {
            game.running = false
            currentLevelIndex ++
        }
        onCurrentLevelChanged: {
            if(currentLevel == null)
                game.isOver = true
        }
    }

    Ball {
        id: ball
        initialX: game.width * 0.5
        initialY: game.height * 0.75
    }
    Dude {
        id: dude
        anchors.bottom: parent.bottom
        maxX: game.width - width
    }

    Timer {
        running:  true
        interval: 100
        onTriggered: {
            parent.forceActiveFocus()
        }
    }
    Keys.onReturnPressed: {
        running = true
        event.accepted = true
    }
    MouseArea {
        anchors.fill: parent
        onClicked:
            game.running = true
    }

//    DebugDraw {
//        id: debugDraw
//        visible: _constants.isDebugBuild
//        world: physicsWorld
//        anchors.fill: parent
//        opacity: 0.75
//    }
}

