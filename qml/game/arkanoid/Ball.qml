
import QtQuick 2.15
import Box2D 2.0
import QtMultimedia 5.15

BallItem {
    id: root

    property real ballSpeed: 4 * mm
    property int initialX
    property int initialY
    x: initialX
    y: initialY

    function startUpwards(angle) {
        var angle = Math.PI * (0.75 - angle * 0.5)
        var speed = root.ballSpeed
        boxBody.linearVelocity = Qt.point(speed * Math.cos(angle), -1 * speed * Math.sin(angle))
    }

    function reset() {
        boxBody.linearVelocity = Qt.point(0, 0)
        root.x = root.initialX
        root.y = root.initialY
    }

    Body {
        id: boxBody

        target: root
        bodyType: Body.Dynamic
        world: physicsWorld
        fixtures: Circle {
            x: root.width / 2
            y: root.height / 2
            id: circle
            objectName: "ball"

            radius: 1 //root.height / 2
            density: 1
            friction: 0.0
            restitution: 1
            onEndContact: {
                if(!game.running)
                    return
                if(other.sensor === true)
                    return
                hitSound.stop()
                hitSound.play()

                var velY = boxBody.linearVelocity.y
                var limit = 3.5
                if(velY < limit && velY > -limit) {     // tento kód zaručuje, že loptička nebude lietať príliš vodorovne
                    var sign = 1.0
                    if(velY !== 0.0)
                        sign = velY/Math.abs(velY)
                    boxBody.linearVelocity = Qt.point(boxBody.linearVelocity.x, sign * 4)
                }
            }
        }
    }
    Audio {
        id: hitSound
        source: _soundPath + "/hit.wav"
    }
}

