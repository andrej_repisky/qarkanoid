<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ" sourcelanguage="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../../qml/game/About.qml" line="22"/>
        <source>
&lt;h2 align=center&gt; qArkanoid &lt;/h2&gt;
&lt;p&gt;
Copyright 2020 Andrej Repiský 
&lt;/p&gt;&lt;p&gt;
This program is free software: you can redistribute it and/or modify
it under the terms of the European Union Public License, version 1.2.
&lt;/p&gt;&lt;p&gt;
This program is distributed in the hope that it will be useful,
but with absolutely no warranty. See the enclosed EUPL for more details.
&lt;/p&gt;
&lt;h3 align=center&gt; Third-party software &lt;/h3&gt;
&lt;p&gt;
This program uses the following third-party libraries:
&lt;ul&gt;
&lt;li&gt;Qt 5.15 (license: GNU GPL, version 3)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://box2d.org/&quot;&gt;Box2D 2.0&lt;/a&gt; (Copyright 2020 Erin Catto)&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;

&lt;h3 align=center&gt; Media &lt;/h3&gt;
&lt;p&gt;Icons are downloaded from &lt;a href=&quot;https://flyclipart.com&quot;&gt;https://flyclipart.com&lt;/a&gt; under Creative Commons BY-NC 4.0 license. &lt;/p&gt;
&lt;p&gt;Sounds are downloaded from &lt;a href=&quot;http://www.freesound.org/people/kantouth/sounds/106727/&quot;&gt;www.freesound.org&lt;/a&gt; under Creative Commons Attribution 3.0 Unported (CC BY 3.0) license. &lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppSettings</name>
    <message>
        <location filename="../../src/utils/AppSettings.cpp" line="18"/>
        <source>Build date and time: %1</source>
        <translation>Datum a čas sestavy: %1</translation>
    </message>
</context>
<context>
    <name>ArkanoidMain</name>
    <message>
        <location filename="../../qml/game/ArkanoidMain.qml" line="43"/>
        <source>Lives</source>
        <translation>Životy</translation>
    </message>
    <message>
        <location filename="../../qml/game/ArkanoidMain.qml" line="62"/>
        <source>Score</source>
        <translation>Skóre</translation>
    </message>
    <message>
        <location filename="../../qml/game/ArkanoidMain.qml" line="70"/>
        <source>Level</source>
        <translation>Úroveň</translation>
    </message>
    <message>
        <location filename="../../qml/game/ArkanoidMain.qml" line="79"/>
        <source>Stretch</source>
        <translation>Zvětšení</translation>
    </message>
</context>
<context>
    <name>CComboBox</name>
    <message>
        <location filename="../../qml/controls/CComboBox.qml" line="20"/>
        <source>vyplnit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CollectItem</name>
    <message>
        <location filename="../../qml/game/arkanoid/CollectItem.qml" line="22"/>
        <source>LIFE</source>
        <translation>ŽIVOT</translation>
    </message>
</context>
<context>
    <name>GameWrap</name>
    <message>
        <location filename="../../qml/game/arkanoid/GameWrap.qml" line="16"/>
        <source>Game over</source>
        <translation>Konec hry</translation>
    </message>
    <message>
        <location filename="../../qml/game/arkanoid/GameWrap.qml" line="23"/>
        <source>Pause</source>
        <translation>Pauza</translation>
    </message>
    <message>
        <location filename="../../qml/game/arkanoid/GameWrap.qml" line="35"/>
        <source>Level %1</source>
        <translation>Úroveň %1</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../../qml/home/HomePage.qml" line="26"/>
        <source>&lt;u&gt;N&lt;/u&gt;ew game</source>
        <translation>&lt;u&gt;N&lt;/u&gt;ová hra</translation>
    </message>
    <message>
        <location filename="../../qml/home/HomePage.qml" line="38"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../../qml/home/HomePage.qml" line="43"/>
        <source>About</source>
        <translation>O hře</translation>
    </message>
    <message>
        <location filename="../../qml/home/HomePage.qml" line="61"/>
        <source>
**Game controls:**  
Enter: starts the ball  
arrow keys are moving the trolley  
Esc, Q: leaves the game  
P: pauses the game  
</source>
        <translation>**Ovládání hry:**  
Enter – odstartuje míč  
šípky – pohybují jezdcem vpravo a vlevo  
Esc, Q – opustí hru  
P – pozastaví hru  </translation>
    </message>
    <message>
        <source>
**Game controls:**  
* Enter: starts the ball
* arrow keys are moving the trolley
* Esc, Q: leaves the game
* P: pauses the game
</source>
        <translation type="vanished">**Ovládání hry:**  
Enter – odstartuje míč  
šípky – pohybují jezdcem vpravo a vlevo  
Esc, Q – opustí hru  
P – pozastaví hru  
</translation>
    </message>
</context>
<context>
    <name>YesNoDialog</name>
    <message>
        <location filename="../../qml/controls/dialogs/YesNoDialog.qml" line="66"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/controls/dialogs/YesNoDialog.qml" line="74"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
