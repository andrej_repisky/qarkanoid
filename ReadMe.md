# qArkanoid
This is a classic brick breaker game, which uses Qt Quick and [Box2D](https://box2d.org/) library. It runs smoothly on the lowest-end hardware. It can be built for GNU/Linux, Windows and Android.

## Download for PC
[Windows (64-bit)](https://www.ulovek.info/qarkanoid/download_win.php)  
Extract the archive on your hard drive and run `qArkanoid.exe` as a regular user.

[GNU/Linux (All distros, 64-bit AppImage)](https://www.ulovek.info/qarkanoid/download_linux.php)  
Run `chmod a+x` on the downloaded file and execute.

## Gameplay
* Enter: starts the ball
* arrow keys are moving the trolley
* Esc, Q: leaves the game
* P: pauses the game

## Screenshots
![](doc/Gameplay_L3.png "Gameplay")
![](doc/Level2.png "Level 2")
![](doc/Level3.png "Level 3")
![](doc/Level4.png "Level 4")
![](doc/Level5.png "Level 5")

## Build instructions

This game requires Qt 5.15, which you can download using the online installer. In addition, you need QML-Box2D:

* Clone QML-Box2D:  
    `git clone https://github.com/qml-box2d/qml-box2d.git`
* Follow the included Readme.md.

Afterwards, you can open qArkanoid.pro in Qt Creator or run `qmake` and `make`.

