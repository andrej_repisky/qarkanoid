
#pragma once

#include <QObject>

class QQmlApplicationEngine;
class QQuickItem;

class MainQml : public QObject
{
    Q_OBJECT
public:
    MainQml(QObject *parent = 0);

    QQuickItem *rootItem() const;
    void showToast(const QString &text) const;

    void startUI();

private:
    QQmlApplicationEngine *m_engine;
};
