
#pragma once

#include <QObject>

#include "src/shared/Singleton.h"
#include "MainQml.h"

class MainObject : public QObject, public Singleton<MainObject>
{
    Q_OBJECT

public:
    MainObject(QObject *parent = 0);

    static const QString appDataPath;

    const MainQml &qml() const { return m_qml; }

private:
    void installTranslator();
    void loadFont();

    MainQml m_qml;
};
