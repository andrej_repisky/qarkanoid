
#include "AppSettings.h"

#include <QGuiApplication>
#include <QProcess>

void AppSettings::restart() const
{
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

QString AppSettings::versionText() const
{
    QString dateTime = QString("%1, %2").arg(__DATE__).arg(__TIME__);

    QString retVal;
    retVal += tr("Build date and time: %1").arg(dateTime);

    return retVal;
}
