

#pragma once

#include <QObject>
#include <QQmlComponent>

class QQuickItem;

class CDialog : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlComponent* content READ content WRITE setContent)
    Q_PROPERTY(bool opened READ opened NOTIFY openedChanged)
    Q_CLASSINFO("DefaultProperty", "content")

public:
    CDialog(QObject *parent = 0);

    QQmlComponent *content() { return m_content; }
    void setContent(QQmlComponent *value) { m_content = value; }
    bool opened() const { return m_topItem != nullptr; }

    Q_INVOKABLE void open();
    Q_INVOKABLE void close();

signals:
    void openedChanged();

private:
    QQmlComponent *m_content;
    QQuickItem *m_topItem;
};

