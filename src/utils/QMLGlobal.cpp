
#include "QMLGlobal.h"
QVariantMap createConstants() {
    QVariantMap retVal;
#ifdef QT_DEBUG
    retVal.insert("isDebugBuild", QVariant(true));
#else
    retVal.insert("isDebugBuild", QVariant(false));
#endif
    retVal.insert("heading_size_mm", 6.5);
    return retVal;
}
