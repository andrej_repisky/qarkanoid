

#include "CDialog.h"
#include "../MainObject.h"
#include "../utils/QMLGlobal.h"

#include <QQuickItem>
#include <QQmlContext>
#include <QQmlEngine>

QML_REGISTER(CDialog)

CDialog::CDialog(QObject *parent) : QObject(parent),
    m_content(nullptr), m_topItem(nullptr)
{ }

void CDialog::open()
{
    if(m_topItem != nullptr)
        return;

    QQmlContext *context = new QQmlContext(QQmlEngine::contextForObject(this));
    context->setContextProperty("_dialog", this);
    m_topItem = qobject_cast<QQuickItem *>(m_content->create(context));

    QQuickItem *rootItem = MainObject::instance().qml().rootItem();
    m_topItem->setParent(this);
    m_topItem->setParentItem(rootItem);
    emit openedChanged();
}

void CDialog::close()
{
    if(m_topItem == nullptr)
        return;
    m_topItem->deleteLater();
    m_topItem = nullptr;
    emit openedChanged();
}
