
#pragma once

#include "src/shared/Settings.h"
#include "src/shared/Singleton.h"

class AppSettings : public Settings, public Singleton<AppSettings>
{
    Q_OBJECT

public:
    AppSettings() : Settings() {}
    Q_INVOKABLE void restart() const;
    Q_INVOKABLE QString versionText() const;
};
