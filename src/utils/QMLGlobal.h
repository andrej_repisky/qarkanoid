

#pragma once

#include <QVariantMap>

QVariantMap createConstants();

const QVariantMap constants = createConstants();

#ifdef QT_QUICK_LIB
#include <QtQml>
#define QML_REGISTER(type) static int unused_val = qmlRegisterType<type>("MyStuff", 1, 0, #type);
#define QML_REGISTER_UNCR_NAMED(type, qmlName) static int unused_val = qmlRegisterUncreatableType<type>("MyStuff", 1, 0, #qmlName, "Type can't be instantiated.");
#define QML_REGISTER_UNCR(type) QML_REGISTER_UNCR_NAMED(type, type)
#else
#define QML_REGISTER(type)
#define QML_REGISTER_UNCR_NAMED(type, qmlName)
#define QML_REGISTER_UNCR(type)
#endif

#define PRINT(a) qDebug() << #a + QString(": ") << a

