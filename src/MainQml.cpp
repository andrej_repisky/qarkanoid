
#include "MainQml.h"
#include "utils/AppSettings.h"

#include "utils/QMLGlobal.h"

#include <QtQml>
#include <QQuickWindow>
#include <QQuickItem>
#include <QScreen>
#include <QTimer>
#include <QGuiApplication>
MainQml::MainQml(QObject *parent) : QObject(parent)
{
    m_engine = nullptr;
}

void MainQml::startUI()
{
    m_engine = new QQmlApplicationEngine(this);
    QQmlContext *context = m_engine->rootContext();

    qreal pxPermm = qApp->primaryScreen()->physicalDotsPerInch() / 25.4;
#ifdef Q_OS_LINUX
    pxPermm = 4.0;
#endif
    qDebug() << "Pixels per mm: " << pxPermm;
    const QString soundPath = "qrc:/sounds";
    context->setContextProperty("mm", pxPermm);
    context->setContextProperty("_soundPath", QVariant(soundPath));
    context->setContextProperty("_settings", &AppSettings::instance());
    context->setContextProperty("_constants", QVariant(constants));

    m_engine->load(QUrl(QStringLiteral("qrc:/qml/home/main.qml")));

    if(m_engine->rootObjects().isEmpty()) { // QML Error
        QTimer::singleShot(200, qApp, SLOT(quit()));
    }
}

QQuickItem *MainQml::rootItem() const
{
    if(m_engine == nullptr || m_engine->rootObjects().isEmpty())
        return nullptr;
    QQuickWindow *window = qobject_cast<QQuickWindow *>(m_engine->rootObjects().first());
    QQuickItem *topItem = window->contentItem();
    if(!topItem)
        return nullptr;

    while(topItem->parentItem())
        topItem = topItem->parentItem();

    return topItem;
}

void MainQml::showToast(const QString &text) const
{
    if(m_engine == nullptr || m_engine->rootObjects().isEmpty())
        return;
    QQuickWindow *window = qobject_cast<QQuickWindow *>(m_engine->rootObjects().first());
    QQuickItem *mainItem = window->findChild<QQuickItem *>("rootGUIItem");
    if(mainItem == nullptr)
        return;

    QVariant returnedValue;
    QMetaObject::invokeMethod(mainItem, "showToast",
        Q_RETURN_ARG(QVariant, returnedValue),
        Q_ARG(QVariant, QVariant(text)));
}

