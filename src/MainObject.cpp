
#include "MainObject.h"
#include "utils/AppSettings.h"

#include <QGuiApplication>
#include <QTranslator>
#include <QFontDatabase>
#include <QtDebug>
#include <QTimer>

#if defined(QT_DEBUG)
const QString MainObject::appDataPath = "../qarkanoid/resources";
#else
const QString MainObject::appDataPath = "./resources";
#endif
MainObject::MainObject(QObject *parent) : QObject(parent)
{
    loadFont();
    installTranslator();
    m_qml.startUI();
}

void MainObject::installTranslator()
{
    QLocale systemLocale = QLocale::system();
    QString langCode = systemLocale.uiLanguages().at(0).left(2);     // Windows: ("de-DE", "en-US")
    if(langCode == "sk")
        langCode = "cs";
    QTranslator *pTranslator = new QTranslator(this);
    pTranslator->load(langCode, ":/translations");
    qApp->installTranslator(pTranslator);
}

void MainObject::loadFont()
{
    int font_id = QFontDatabase::addApplicationFont(appDataPath + "/fonts/Ubuntu-R.ttf");
    QStringList families = QFontDatabase::applicationFontFamilies(font_id);
    if(families.isEmpty()) {
        qWarning() << "Failed to load font.";
        return;
    }
    QFont font(families.first(), 11);
    qApp->setFont(font);
}

