
#include "MainObject.h"

#include <QGuiApplication>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    MainObject::instance();

    return app.exec();
}
