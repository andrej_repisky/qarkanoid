
#pragma once

#include <QObject>
#ifdef QT_QML_LIB
#include <QJSValue>
#endif
#include <QFileInfo>
#include <QString>
#include <QVariant>

/*!
  \author Andrej Repiský
  \date   11. 12. 2015
  \brief  Nastavenia

This is an alternative to QSettings, which uses JSON format for storage.

Example usage in QML – values are updated when settings.setValue() is called:

    Label {
        text: settings.value("title" + settings.emptyString)
    }
*/

class Settings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString emptyString READ getEmptyStr NOTIFY valueChanged)

public:
    Settings(QObject *parent = 0);

    Q_INVOKABLE QVariant value(QString key, const QVariant &defaultVal);    //!< WARNING: This will insert a new key value pair if it doesn't exist.
    Q_INVOKABLE QVariant value(QString key) const;
    void setValue(QString key, const QVariant &value);
#ifdef QT_QML_LIB
    Q_INVOKABLE void setValue(QString key, const QJSValue &value) { setValue(key, value.toVariant()); }
#endif

    QString getEmptyStr() const { return QString(""); }
    QFileInfo file() const { return m_settingsFile; }

signals:
    void valueChanged();

private:
    void save();

    const QFileInfo m_settingsFile;
    QVariantMap m_settings;
};

